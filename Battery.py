#!/usr/bin/env python

import time
import pigpio
import serial

class Slider:
    
    def __init__(self, controller_connection):

        # Sliders 1,2 on Sabertooth SER=16003F5D76EA
        # Slider 3 on Sabertooth SER=16006D4CE9D9
        
        
        self._controller_connection = controller_connection
        self._slider_position = 0
        self._current_target_speed = 0
             

       

 
   
    @property
    def batteryLevel(self):
        try:
            
            timer_start = time.time()
            
            battery_level = ''
            
            self._battery_cmd = self._slider_id + ",getp" + "\r\n"
                      
            read_data = self._controller_connection.read_cmd(self._slider_cmd)
            
            position_list = str(read_data[1],'utf-8').split("\n")
            for x in position_list :
                if x.startswith(self._slider_id) and x.endswith("\r"):
                    kangaroo_pos = x.lower().replace(self._slider_id + ",p","")                   
                    print("kangaroo pos", kangaroo_pos)
                    break         

            timer_end = time.time()
            print("Sliders Serial Read duration: ", timer_end-timer_start)
           
            self._slider_position = int(float(kangaroo_pos)) - 300
            return self._slider_position
            
        except:
            print("Error retrieving slider position: ", kangaroo_pos)
            return 0


_pi = pigpio.pi("192.168.1.103", 8888)

if _pi:
    print("connected")
_battery_handle = _pi.serial_open("/dev/ttyAMA0", 115200)
print("open")
_battery_command_serial = "M5:getb\r\n"
if _battery_command_serial:
    print(_battery_command_serial)
    cmd_success = _pi.serial_write(_battery_handle, "M5:getb\r\n")
    (b,d) = _pi.serial_read(_battery_handle, 1000)
_pi.serial_close(_battery_handle)
print("closed")
battery_level = "a"
for x in d: 
    battery_level = str(ord(x))


if battery_level:
    print("bat lev:" + str(battery_level))

print("closed")

#class Battery:
#    """description of class"""


#    #Battery life cycle is from 3.3V to 3.1
#    #Battery life cycle used 3.25 to 3.15

   
#    def __init__(self):
       
  
#        self._pi = pigpio.pi("192.168.1.101", 8888)
#        self._battery = self._pi.serial_open("/dev/ttyAMA0", 115200, 0)
#        print("born")

#    def closeSerial(self):
#        self._pi.serial_close(self._battery)

#    @property
#    def remaining(self):
#        try:         
#            self._battery_command_serial = "M1:getb\r\n"
#            print(self._battery_command_serial)
#            self._remaining = self._pi.serial_write(self._battery, self._battery_command_serial)
#            print(self._remaining)
#            #return self._remaining
#        except:
#            raise Exception("remaining not defined")
#B1= Battery()
#result = B1.remaining
#print(result)
#B1.closeSerial
