#!/usr/bin/env python

import time
#import pigpio # http://abyz.co.uk/rpi/pigpio/python.html
import serial
from Slider import *
from ControllerConnection import *

controllerCon1 = ControllerConnection("1")
controllerCon2 = ControllerConnection("2")


slider1 = Slider("1", controllerCon1)
slider2 = Slider("2", controllerCon1)
slider3 = Slider("3", controllerCon2)
time.sleep(1)


for i in range(30):
    slider1.targetSpeed = 100
    slider2.targetSpeed = 80
    slider3.targetSpeed = 60
    time.sleep(0.1)
    print("end position", slider1.sliderPosition)
    print("end position", slider2.sliderPosition)
    print("end position", slider3.sliderPosition)
    # slider1.targetSpeed = -100
    # slider2.targetSpeed = -100
    # slider3.targetSpeed = -100
    # time.sleep(3)

slider1.targetSpeed = 0
slider2.targetSpeed = 0
slider3.targetSpeed = 0

#set a speed

# speed = "10"
# for i in range(3):
    # i=i+1
    # position_slider = str(i) + ",s" + speed + "\r\n"
    # position_slider_bytes = position_slider.encode("utf-8")
    # if i ==1:
        # slider1._slider_serial.write(position_slider_bytes)
    # elif i ==2:
        # slider2._slider_serial.write(position_slider_bytes)
    # elif i==3:
        # slider3._slider_serial.write(position_slider_bytes)
      
# time.sleep(10)

# print("end position", slider1.sliderPosition)
# print("end position", slider2.sliderPosition)
# print("end position", slider3.sliderPosition)

controllerCon1.close_serial_port()
controllerCon2.close_serial_port()




