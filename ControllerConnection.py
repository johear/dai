#!/usr/bin/env python

import time
import pigpio
import serial




class ControllerConnection:

    def __init__(self, controller_id):

        # Sliders 1,2 on Sabertooth SER=16003F5D76EA / controller_id = 1
        # Slider 3 on Sabertooth SER=16006D4CE9D9 / controller_id = 2
        
        self._controller_id = controller_id
        self._hostname = {"1": "192.168.1.112", "2": "192.168.1.113"} 
        
        try:      
            # Connect to remote Pi4 using pigpio
            self._pi = pigpio.pi(self._hostname[self._controller_id],8888)
        except Exception as e: print(e)

        try:    
            # Connect remote Pi4 to motor controller using serial UART / TTY
            self._controller_connection = self._pi.serial_open("/dev/ttyAMA1", 9600) # usb : /dev/tty-Slider
        except Exception as e: print(e)
       
        
    def open_serial_port(self):
        try:      
            # Connect to remote Pi4 using pigpio
            self._pi = pigpio.pi(self._hostname[self._controller_id],8888)
        except Exception as e: print(e)

        try:      
            # Connect remote Pi4 to motor controller using serial UART / TTY
            self._controller_connection = self._pi.serial_open("/dev/ttyAMA1", 9600) # usb : /dev/tty-Slider
        except Exception as e: print(e)
       

    def write_cmd(self, cmd):
        
        try:
            
            self._cmd = cmd
                     
            try:
                self._pi.serial_write(self._controller_connection, self._cmd)
                
            except:
                print("write_cmd failed on controller :", self._controller_id)
                self.open_serial_port()
                print("Opened new port successfully for controller: ", self._controller_id)
                           
        except:
            raise Exception("Couldn't write command to motor controller")

   
    def read_cmd(self, cmd):
        try:
            self._cmd = cmd   
            self.write_cmd(self._cmd)
            read_length = 0
            read_data = None                   
            x = 0
            read_length = self._pi.serial_data_available(self._controller_connection)
            while read_length < 0 and x < 110 :
                read_length = self._pi.serial_data_available(self._controller_connection)
                # print("bytes available to read : ", read_length)
                x=x+1
                if x > 50 :
                    self.write_cmd(self._cmd)
                    print("LOOPING: ", x)
            if read_length > 0 :
                read_data = self._pi.serial_read(self._controller_connection, read_length)
                self._controller_data = read_data
            # else :
                # self._controller_data = "NoData"
            print("controller data : ", self._controller_id, " : ", self._controller_data)
            return self._controller_data
            
        except:
            print("Error retrieving controller data: ", self._controller_data)
            return 0


    def close_serial_port(self):
        self._pi.serial_close(self._controller_connection)
 

