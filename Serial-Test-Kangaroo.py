#!/usr/bin/env python


import serial
import time
import pigpio # http://abyz.co.uk/rpi/pigpio/python.html
axis1 = "1"
axis2 = "2"
axis3 = "3"
pi12 = pigpio.pi()
pi3 = pigpio.pi("192.168.11.106",8888)
motor12 = pi12.serial_open("/dev/ttyACM0", 9600)
motor3 = pi3.serial_open("/dev/ttyAMA0", 9600)


################## START AND HOME ###########################
try:

	start_slider = axis1 + ",start" + "\r\n"
	pi12.serial_write(motor12, start_slider)
	#time.sleep(3)
	home_slider = axis1 + ",home" + "\r\n"
	pi12.serial_write(motor12, home_slider)
	#time.sleep(10)

	# start_slider = axis2 + ",start" + "\r\n"
	# pi12.serial_write(motor12, start_slider)


	# home_slider = axis2 + ",home" + "\r\n"
	# pi12.serial_write(motor12, home_slider)
	# #time.sleep(10)


	# start_slider = axis3 + ",start" + "\r\n"
	# pi3.serial_write(motor3, start_slider)

	# home_slider = axis3 + ",home" + "\r\n"
	# pi3.serial_write(motor3, home_slider)
	
	time.sleep(10)

	################### MOVE SLIDERS ###########################

	Pos_speed = ",p200s50"
	
	move_slider = axis1 + Pos_speed + "\r\n"
	pi12.serial_write(motor12, move_slider)


	# move_slider = axis2 + Pos_speed + "\r\n"
	# pi12.serial_write(motor12, move_slider)
	

	# move_slider = axis3 + Pos_speed + "\r\n"
	# pi3.serial_write(motor3, move_slider)
	# time.sleep(2)
	# Pos_speed = ",pi50s50"
	
	# move_slider = axis1 + Pos_speed + "\r\n"
	# pi12.serial_write(motor12, move_slider)


	# move_slider = axis2 + Pos_speed + "\r\n"
	# pi12.serial_write(motor12, move_slider)
	

	# move_slider = axis3 + Pos_speed + "\r\n"
	# pi3.serial_write(motor3, move_slider)
	

	##################### POWER DOWN ###########################

	# powerdown_slider = axis1 + ",powerdown" + "\r\n"
	# pi12.serial_write(motor12, powerdown_slider)
	# print(pi12.serial_write(motor12, powerdown_slider))

	# powerdown_slider = axis2 + ",powerdown" + "\r\n"
	# pi12.serial_write(motor12, powerdown_slider)
	# print(pi12.serial_write(motor12, powerdown_slider))

	# powerdown_slider = axis3 + ",powerdown" + "\r\n"
	# pi3.serial_write(motor3, powerdown_slider)
	# print(pi3.serial_write(motor3, powerdown_slider))


	#################### GET COMMANDS ##########################

	_slider_id = axis1
	_pi = pi12
	_pi_serial = motor12
			
	end_char = ''
	kangaroo_pos = ''
	slider_cmd = _slider_id + ",getp" + "\r\n"
	_pi.serial_write(_pi_serial, slider_cmd)
	while end_char != '\n':
		read_length = _pi.serial_data_available(_pi_serial)
		if read_length > 0:
			read_data = _pi.serial_read(_pi_serial, read_length)
			print(read_data)
			kangaroo_pos += str(read_data[1],'utf-8')
			end_char = kangaroo_pos[len(kangaroo_pos) - 1]
	print(kangaroo_pos)
	kangaroo_pos = kangaroo_pos.replace('p', '').replace("\r\n", '')
	print(kangaroo_pos)
	_slider_position = float(kangaroo_pos)
	print(_slider_position)
	

	# get_pos = axis1 + ",getp" + "\r\n"
	# pi12.serial_write(motor12, get_pos)
	
	# get_max_pos = axis1 + ",getmax" + "\r\n"
	# pi12.serial_write(motor12, get_max_pos)
	
	# get_speed = axis1 + ",gets" + "\r\n"
	# pi12.serial_write(motor12, get_speed)
	# time.sleep(5)
	
	# get_pos = axis1 + ",getp" + "\r\n"
	# pi12.serial_write(motor12, get_pos)

	# start_time = time.time()
		
	# while time.time() - start_time < 2:
	# 	data_count = pi12.serial_data_available(motor12)
	# 	if data_count > 0:
	# 		data = pi12.serial_read(motor12, data_count)
	# 		print(data)
		# time.sleep(0.1)
		
	# # get_pos = axis3 + ",getp" + "\r\n"
	# # pi3.serial_write(motor3, get_pos)

	# get_max_pos = axis3 + ",getmax" + "\r\n"
	# pi3.serial_write(motor3, get_max_pos)

	# start_time = time.time()
		
	# while time.time() - start_time < 2:
		# data_count = pi3.serial_data_available(motor3)
		# if data_count > 0:
			# print(data)
			# data = pi3.serial_read(motor3, data_count)
			# print(data)
		# # time.sleep(0.1)
		
	####################### CLOSE SERIAL ########################

	pi12.serial_close(motor12)
	pi3.serial_close(motor3)
except:
	pi12.serial_close(motor12)
	pi3.serial_close(motor3)
	raise


