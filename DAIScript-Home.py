#!/usr/bin/env python

from Wheel import Wheel
from Slider2 import Slider2
import time

print("starting")

w1a = Wheel("1a")
w2a = Wheel("2a")
w3a = Wheel("3a")
w1b = Wheel("1b")
w2b = Wheel("2b")
w3b = Wheel("3b")

S1=Slider2("1")
S2=Slider2("2")
S3=Slider2("3")

w1a.targetSpeed = 0
w2a.targetSpeed = 0
w3a.targetSpeed = 0
w1b.targetSpeed = 0
w2b.targetSpeed = 0
w3b.targetSpeed = 0
S1.targetSpeed = 0
S2.targetSpeed = 0
S3.targetSpeed = 0

w1a.closeSerial()
w2a.closeSerial()
w3a.closeSerial()
w1b.closeSerial()
w2b.closeSerial()
w3b.closeSerial()

#w1a.freewheelMode = 0
#w1a.wheelIsOnGround = 1
#print(w1a.currentTorque)
